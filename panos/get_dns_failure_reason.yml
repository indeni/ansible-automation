# CLI usage example:
# ansible-playbook  get_dns_failure_reason.yml -e "dns_server=8.8.8.8"
#
# Input parameters:
#   dns_server: dns server address in the alert data; if not supplied, the default value will be retrieved from the device
#
# Output:
#   the last executed task indeni_conclusion will output a json object which contains "triage_conclusion", "triage_has_conclusion", "triage_remediation_steps", "triage_has_remediation_steps". If triage_has_conclusion is true, triage_conclusion will contain the conclusion; if triage_has_remediation_steps is true, triage_remediation_steps will contain the remediation_steps
#   sample indeni_conclusion output:
#     {
#      "changed": false,
#      "status": "success",
#      "triage_conclusion_title": "dns server unreachable",
#      "triage_conclusion": "The dns server is unreachable",
#      "triage_has_conclusion": true,
#      "triage_has_remediation_steps": true,
#      "triage_remediation_steps": "Please check if this dns server is online and has the correct ip address defined"
#     }
#
# Dependencies:
#    apt install nmap
#    pip install lxml


- name: find the reason why a dns service is unreachable 
  hosts: localhost
  gather_facts: False

  tasks:
  - name: get dns server address
    panos_set:
      ip_address: '{{ip_address}}'
      username: '{{username}}'
      password: '{{password}}'
      xpath: '/config/devices/entry/deviceconfig/system/dns-setting/servers'
      command: 'show'
    register: dns_server_result
    ignore_errors: yes
  - name: indeni_step get dns server address
    indeni_step:
      task_description: "on remote device execute command 'show config running xpath devices/entry/deviceconfig/system/dns-setting/servers'"
      task_type: "ACTION"
      task_full_command: "show config running xpath devices/entry/deviceconfig/system/dns-setting/servers"
      task_conclusion: "{{ 'command execution failed' if dns_server_result.failed else 'xml response received, need further parse' }}"
      task_full_output: "{{dns_server_result}}"
  - xml:
      xmlstring: '{{dns_server_result.stdout_xml}}'
      xpath: '/response/result/servers/primary'
      content: text
    register: prime_address
    ignore_errors: yes
  - xml:
      xmlstring: '{{dns_server_result.stdout_xml}}'
      xpath: '/response/result/servers/secondary'
      content: text
    register: second_address
    ignore_errors: yes
  - set_fact:
      dns_server: '{{prime_address.matches[0].primary}}'
    when: dns_server is undefined and not prime_address.failed
  - set_fact:
      dns_server: '{{second_address.matches[0].secondary}}'
    when: dns_server is undefined and not second_address.failed
  - name: indeni_step parse xml response for dns server address
    indeni_step:
      task_description: "{{'successfully parsed xml response for dns server address' if not prime_address.failed or not second_address.failed else 'dns server address is supplied to the playbook'}}"
      task_type: "ACTION"
      task_full_command: "xml parse /response/result/servers"
      task_conclusion: "dns server address is {{dns_server}}"
      task_full_output: ""
    when:  dns_server is defined
  - name: dns server address not provided
    block:
      - indeni_conclusion:
          triage_conclusion_title: dns server address not provided
          triage_conclusion: >
            The dns server address is not provided.
          triage_has_conclusion: true
          triage_remediation_steps: >
            Please make sure dns server is configued on the device, or supply that information to ansible playbook.
          triage_has_remediation_steps: true
      - meta: end_play
    when: dns_server is undefined
  - name: check if remote dns port is open
    shell: |
      nmap -sU -p 53 {{dns_server}} | awk '/domain/{print $2}'
    become: true
    register: nmap_output
    ignore_errors: yes
  - name: indeni_step test remote dns port
    indeni_step:
      task_description: "test remote dns port using command 'nmap -sU -p 53 {{dns_server}}'"
      task_type: "ACTION"
      task_full_command: "nmap -sU -p 53 {{dns_server}}"
      task_conclusion: "{{'command execution failed' if nmap_output.failed else 'nmap output received, need further parsing'}}"
      task_full_output: "{{nmap_output if nmap_output is defined}}"
  - name: dns service port reachable
    block:
      - name: indeni_step check if remote dns port is open
        indeni_step:
          task_description: "check if remote dns port is open"
          task_type: "DECISION"
          task_full_command: "test nmap_output.stdout == 'open'"
          task_conclusion: "YES"
          task_full_output: ""
      - indeni_conclusion:
          triage_conclusion_title: 'dns server reachable'
          triage_conclusion: >
            dns server port is open and reachable
          triage_has_conclusion: true
          triage_remediation_steps: >
            The dns serer is online and reachable. Please open a support ticket with
            Palo Alto support center for solution.
          triage_has_remediation_steps: true
      - meta: end_play
    when:  nmap_output.stdout == 'open'
  - name: indeni_step check if remote dns port is open
    indeni_step:
      task_description: "check if remote dns port is open"
      task_type: "DECISION"
      task_full_command: "test nmap_output.stdout == 'open'"
      task_conclusion: "NO"
      task_full_output: ""
    when: nmap_output.stdout != 'open'
  - name: check DNS server network reachability
    linux_icmp_ping:
      host: '{{dns_server}}'
    ignore_errors: true
    register: ping_output
  - name: indeni_step ping NTP server
    indeni_step:
      task_description: "ping dns server {{dns_server}}"
      task_type: "ACTION"
      task_full_command: "ping {{dns_server}}"
      task_conclusion: "ping complete, need further parsing"
      task_full_output: "{{ping_output}}"
  - name:  dns server reachable
    block:
      - name: indeni_step check if dns server is pingable
        indeni_step:
          task_description: "check if dns server is pingable"
          task_type: "DECISION"
          task_full_command: "not ping_output.failed|bool"
          task_conclusion: "YES"
          task_full_output: ""
      - indeni_conclusion:
          triage_conclusion_title: dns server reachable
          triage_conclusion: >
            The dns server is reachable, but the dns port is not open.
          triage_has_conclusion: true
          triage_remediation_steps: >
            Please check if the configured dns server has dns service running.
            If the dns service operate properly, please check if some firewall
            is blocking the dns traffic.
          triage_has_remediation_steps: true
      - meta: end_play
    when: not ping_output.failed|bool
  - name:  dns server unreachable
    block:
      - indeni_step:
          task_description: "check if dns server is pingable"
          task_type: "DECISION"
          task_full_command: "ping_output.failed|bool"
          task_conclusion: "NO"
          task_full_output: ""
      - indeni_conclusion:
          triage_conclusion_title: dns server unreachable
          triage_conclusion: >
            The dns server is unreachable.
          triage_has_conclusion: true
          triage_remediation_steps: >
            Please check if this dns server is online and has the correct
            ip address defined.
          triage_has_remediation_steps: true

