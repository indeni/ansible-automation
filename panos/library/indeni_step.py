#!/usr/bin/python
  
ANSIBLE_METADATA = {'metadata_version': '1.1',
                    'status': ['preview'],
                    'supported_by': 'community'}

from ansible.module_utils.basic import AnsibleModule

def main():
    argument_spec = dict(
        task_description=dict(required=True),
        task_type=dict(required=True),
        task_full_command=dict(required=True),
        task_conclusion=dict(required=True),
        task_full_output=dict(required=False, default="")
    )
    module = AnsibleModule(argument_spec=argument_spec, supports_check_mode=False)
    task_description = module.params['task_description']
    task_type = module.params['task_type']
    task_full_command = module.params['task_full_command']
    task_conclusion = module.params['task_conclusion']
    task_full_output = module.params['task_full_output']
    module.exit_json(status="success", task_description=task_description, task_type=task_type, task_full_command=task_full_command, task_conclusion=task_conclusion, task_full_output=task_full_output)


if __name__ == '__main__':
    main()

